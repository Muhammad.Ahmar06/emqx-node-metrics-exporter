FROM node:13
WORKDIR /app

COPY package.json /app
RUN npm install
COPY . /app

CMD node index.js
EXPOSE 9001

COPY docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]