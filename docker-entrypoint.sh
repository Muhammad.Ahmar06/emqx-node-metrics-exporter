#!/bin/sh
#!/bin/sh
set -e

EMQX_HOST=$(hostname -i |grep -E -oh '((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])'|head -n 1)
export EMQX_HOST
echo $EMQX_HOST

exec "$@"
